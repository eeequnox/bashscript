#!/bin/bash

# !!! Please, only sudo user can start this script !!!

echo "Hello, you launched a new script"

while true; do
    read -p "Do you wish to start? (Y/N): " answer
    case $answer in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer Y or N.";;
    esac
done

echo "Starting..."


#If dir files_previous exists then check if file interfaces_previous exist
#else create new one
if [ -d "files_previous" ]; then
    echo "files_previous does exist."
    if [ -f "files_previous/interfaces_previous" ]; then
        echo "interfaces_previous already exists"
	rm interface_previous
	echo "interfaces_previous is deleted"
	echo "creating new interfaces_previous..."
	cp /etc/network/interfaces files_previous/interfaces_previous
    else
        #Copy and save previous settings 
        cp /etc/network/interfaces files_previous/interfaces_previous
    fi
else
    #Create dir for previous interface file (just in case)
    mkdir files_previous
    #Copy and save previous settings 
    cp /etc/network/interfaces files_previous/interfaces_previous
fi

#If enp0s8 already configured in /etc/network/interfaces then break
if grep -q enp0s8 /etc/network/interfaces;
then
    echo "Here are the Strings with the Pattern enp0s8:"
    echo -e "$(grep enp0s8 /etc/network/interfaces)\n"
    echo "Continue..."
else
    #Add new strokes to /etc/network/interfaces
    echo "allow-hotplug enp0s8" >> /etc/network/interfaces
    echo "iface enp0s8 inet static" >> /etc/network/interfaces
    echo -e "\taddress 192.168.1.1" >> /etc/network/interfaces
    echo -e "\tnetmask 255.255.255.0" >> /etc/network/interfaces
    echo -e "\tnetwork 192.168.1.0" >> /etc/network/interfaces
    echo -e "\tdns-nameservers 77.88.8.1 8.8.8.8" >> /etc/network/interfaces
fi


#Raise the network interface
echo "Raising enp0s8 interface..."
ifup enp0s8

echo "Installing DNS-server..."

apt install bind9 bind9utils swaks
systemctl start bind9

#Check status DNS
if (systemctl -q is-active bind9)
then
	echo "DNS bind9 is active"
else
	echo "DNS bind9 is not active"
fi

#Restart network
echo "Service networking is restarting..."
service networking restart

#Gateway configure
# c == 2 then dont add new strokes (net.ipv4.ip_forward is already 1)
# 2 because #net.ipv4.ip_forward=1 and net.ipv4.ip_forward=1 (without comment)
FOUND=`fgrep -c "net.ipv4.ip_forward=1" /etc/sysctl.conf`
# c == 2
if [ $FOUND -eq 2 ];
then
    echo "Here are the Strings with the Pattern net.ipv4.ip_forward=1:"
    echo -e "$(grep net.ipv4.ip_forward=1 /etc/sysctl.conf)\n"
    echo "Continue..."
else
    # c < 2
    if [ $FOUND -lt 2 ];
    then
        echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
        echo "Continue..."
    else
        echo "Too many..."
    fi
fi
#Apply changes without reload
sysctl -p


#Install iptables
echo "Installing iptables..."
apt-get install iptables-persistent

#add new rule and  check ifExist
if grep -q "iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE" /etc/iptables/rules.v4;
then
    echo "Here are the Strings with the Pattern iptables:"
    echo -e "$(grep "iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE" /etc/iptables/rules.v4)\n"
    echo "Continue..."
else
    echo "Add iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE to /etc/iptables/rules.v4"
    #Add new stroke to /etc/iptables/rules.v4
    echo "iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE" >> /etc/iptables/rules.v4
fi

#Save changes
iptables-save

#For saving after reload
apt-get install netfilter-persistent
netfilter-persistent save


#Install DHCP-server
apt-get install isc-dhcp-server
#Copy previous settings /etc/dhcp/dhcpd.conf
if [ -f "files_previous/dhcpd.previous.conf" ]; then
    echo "dhcpd.previous.conf already exists"
    #delete old file if exist, and copy new conf
    rm -r files_previous/dhcpd.previous.conf
    echo "dhcpd.previous.conf is deleted"
    cp /etc/dhcp/dhcpd.conf files_previous/dhcpd.previous.conf
    echo "new dhcpd.previous.conf created"
else
    #Copy and save previous settings 
    cp /etc/dhcp/dhcpd.conf files_previous/dhcpd.previous.conf
fi

#Copy previous settings /etc/default/isc-dhcp-server
if [ -f "files_previous/isc-dhcp-previous-server" ]; then
    echo "dhcpd.previous.conf already exists"
    #delete old file if exist, and copy new conf
    rm -r files_previous/isc-dhcp-previous-server
    echo "isc-dhcp-previous-server is deleted"
    cp /etc/default/isc-dhcp-server files_previous/isc-dhcp-previous-server
    echo "new isc-dhcp-previous-server created"
else
    #Copy and save previous settings 
    cp /etc/default/isc-dhcp-server files_previous/isc-dhcp-previous-server
fi


#add new string and check ifExist
if grep -q "INTERFACESv4=\"enp0s8\"" /etc/default/isc-dhcp-server;
then
    echo "Here are the Strings with the Pattern INTERFACESv4=\"enp0s8\":"
    echo -e "$(grep "INTERFACESv4=\"enp0s8\"" /etc/default/isc-dhcp-server)\n"
    echo "Continue..."
else
    echo "Add INTERFACESv4=\"enp0s8\" to /etc/default/isc-dhcp-server"
    #Add new string to /etc/iptables/rules.v4
    echo "INTERFACESv4=\"enp0s8\"" >> /etc/default/isc-dhcp-server
fi


#Clear file /etc/dhcp/dhcpd.conf
echo "Clearing dhcpd.conf..."
> /etc/dhcp/dhcpd.conf

echo "Configuring dhcpd.conf..."
#Configure (add new strings) dhcpd.conf
echo "option domain-name \"WORKGROUP\";" >> /etc/dhcp/dhcpd.conf
echo "option domain-name-servers 8.8.8.8;" >> /etc/dhcp/dhcpd.conf
echo "default-lease-time 600;" >> /etc/dhcp/dhcpd.conf
echo "max-lease-time 7200;" >> /etc/dhcp/dhcpd.conf
echo "subnet 192.168.1.0 netmask 255.255.255.0 {" >> /etc/dhcp/dhcpd.conf 
echo -e "\trange 192.168.1.10 192.168.1.20;" >> /etc/dhcp/dhcpd.conf
echo -e "\toption broadcast-address 192.168.1.255;" >> /etc/dhcp/dhcpd.conf
echo -e "\toption routers 192.168.1.1;" >> /etc/dhcp/dhcpd.conf
echo -e "\toption domain-name-servers 8.8.8.8;" >> /etc/dhcp/dhcpd.conf
echo "}" >> /etc/dhcp/dhcpd.conf

sleep 5s
#Start dhcp
/etc/init.d/isc-dhcp-server start

#Check status DHCP
if (systemctl -q is-active isc-dhcp-server)
then
    echo "DHCP is active"
    #Check given ip
    echo -e "Checking given ip:\n"
    sleep 5s
    dhcp-lease-list
else
    echo "DHCP is not active"
fi

echo "End of script"
